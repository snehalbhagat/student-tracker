package com.muktangan.ffg.muktangan.dashboard;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.muktangan.ffg.muktangan.R;

public class ProfileActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
    }
}
